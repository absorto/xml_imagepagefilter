We wanted to create a mirror from a medium size wiki that we didn't had access
to the databases. So we used [dumpGenerator.py from WikiTeam](https://github.com/WikiTeam/wikiteam/blob/master/dumpgenerator.py)
to create a dump from the wiki. After this we imported the files with `importDump.php`
and `importImages.php`. The import reported no errors, but about half the images
were not showing in the pages.

We posted the issue on the __WikiTech l__ mailling list and waited to see if anybody
got some ideas on how to deal with this.

[WikiTech l - List](https://lists.wikimedia.org/pipermail/wikitech-l/2018-November/091129.html)

We modified the original xml with this script [xml_filter.py](../blob/master/xml_filter.py)
in order to remove all image descriptions from it. This way when we imported again,
importImages.php would make it's own description pages that would all work properly.

Removing the descriptions solved the problem with the images but we still needed
to add the descriptions back. Initially we thought about adding them manually,
but there were far too many. So we runned a modified version from the script
originally used to remove the descriptions, but this time to create a file that
contained only descriptions [xml_filterOutputuArquivo.py](../blob/master/xml_filterOutputArquivo.py#L50).
We then imported this file to add the descriptions.

As we can see in this image:
![alt text](modified-revisions.jpg "Modified Revisions Screen Shot" ) the descriptions were not showing because there
was a conflict in the timestamps. The timestamp created by the `importImages.php`
script was newer than the original timestamp from the last revision. To fix this
we adjusted the most recent revision of every page in the images description so
they're newer than the revision made by the maintenance scripts. This change was
made using the [xml_filterOutputArquivo.py](../blob/master/xml_filterOutputArquivo.py)
script.

### Update

All the functionalities that were originally split between multiple scripts are now merged in `xml_filter.py` and accessible with options.
For description removal, the option is `remove`. For creating a file with the descriptions only, use the option `desc`. And, finally, to alter timestamps use the `timestamp` option.

