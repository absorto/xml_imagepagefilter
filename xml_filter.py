#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys, os
import re

''' Format MediaWiki dump xml for importing '''

# Process and store command line arguments
if len(sys.argv) < 4:
    print("This script expects 3 arguments, 'dump path', 'new file path' and 'option'")
    sys.exit()
else:
    dump = open((os.getcwd() + '/' + sys.argv[1]), 'r')
    newDump = open((os.getcwd() + '/' + sys.argv[2]), 'w')
    options = ['timestamp', 'remove', 'desc']
    option = sys.argv[3].strip().lower()
    if option not in options:
        print("Option not recognized, options available are 'desc', 'timestamp' and 'remove'")
        sys.exit()

p = re.compile('^\s*<page>', re.IGNORECASE) # page beginning
m = re.compile('^\s*<\/page>', re.IGNORECASE) # page ending
n = re.compile('<title>Arquivo', re.IGNORECASE) # Arquivo filter
ts = r'(\d\d\d\d)-(\d\d)-(\d\d)\w(\d\d:\d\d):\d\dZ' # format pattern

buff = ''
inPage = False
for line in dump:

    # Page beginning or within page declaration
    if p.search(line) or inPage == True:
        inPage = True
        buff += line

        # End of page declaration
        if m.match(line):
            inPage = False

            # Contains 'Arquivo' tag
            if n.search(buff):

                # Replace last timestamp
                if option == 'timestamp':
                    replace = '\\4, \\3/\\2/\\1'
                    buff = re.sub(formatPat, replace, buff, 0, re.MULTILINE | re.DOTALL)
                if option == 'timestamp' or option == 'desc':
                    nDump.write(buff)

            # Write to dump if page doesn't contain 'Arquivo'
            else:
                if option == 'remove':
                    newDump.write(buff)
            buff = ''

    # Line is content
    else:
        newDump.write(line)

newDump.close()
dump.close()

